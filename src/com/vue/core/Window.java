package com.vue.core;

import org.lwjgl.glfw.*;
import org.lwjgl.glfw.GLFWWindowSizeCallback.SAM;
import org.lwjgl.opengl.*;

import com.vue.core.math.Vector2f;

import java.nio.ByteBuffer;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

public class Window
{
	// We need to strongly reference callback instances.
    private GLFWErrorCallback errorCallback;
 
    // The window handle
    private long window;
    
    private int windowWidth;
    private int windowHeight;
    private int windowAspect;
    
    Vector2f screenSize;
    
    boolean vsync;
    
    public void createWindow(boolean isVsync)
    {
    	vsync = isVsync;
    	init();
    }
    
    public long getWindow()
    {
    	return window;
    }
    
    public void setToClose()
    {
    	glfwSetWindowShouldClose(window, GL_TRUE);
    }
    
    private void init()
    {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        glfwSetErrorCallback(errorCallback = errorCallbackPrint(System.err));
 
        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( glfwInit() != GL11.GL_TRUE )
            throw new IllegalStateException("Unable to initialize GLFW");
 
        // Configure our window
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GL_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE); // the window will be resizable
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
        
        // Get the resolution of the primary monitor
        ByteBuffer vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        
        windowWidth = GLFWvidmode.width(vidmode) / 2;
        windowHeight = GLFWvidmode.height(vidmode) / 2;
        screenSize = new Vector2f(windowWidth, windowHeight);
        
        windowAspect = windowWidth / windowHeight;
        
        // Create the window
        window = glfwCreateWindow(windowWidth, windowHeight, "Vue Engine 0.0.1", NULL, NULL);
        if (window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");
        
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
 
        // Center our window
        glfwSetWindowPos(window, (GLFWvidmode.width(vidmode) - windowWidth) / 2, (GLFWvidmode.height(vidmode) - windowHeight) / 2);
        
        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        if(vsync)
        {
            glfwSwapInterval(1);
        }
        else
        {
            glfwSwapInterval(0);
        }
 
        // Make the window visible
        glfwShowWindow(window);
    }
    
    public void destroy()
    {
    	try
    	{
    		// Release window and window callbacks
    		glfwDestroyWindow(window);
    	} 
    	finally 
    	{
    		// Terminate GLFW and release the GLFWerrorfun
    		glfwTerminate();
    		errorCallback.release();
    	}
    }
	
	public int GetAspect()
	{
		return windowAspect;
	}
	
	public int isCloseRequested()
	{
		return glfwWindowShouldClose(window);
	}
	
	public Vector2f getScreenSize()
	{
		return screenSize;	
	}
}
