package com.vue.core.input;

import static org.lwjgl.glfw.GLFW.*;

import com.vue.core.MainComponent;
import com.vue.core.math.Vector2f;

public class InputHandler
{
    private KeyboardHandler keyCallback;
    private MousePosInputHandler mousePosCallback;
    private MouseButtonInputHandler mouseButtonCallback;
    
    private Vector2f mouseLocation;
	
	public InputHandler(long window, MainComponent inMain)
	{
        // Sets our keycallback to equal our newly created Input class()
        glfwSetKeyCallback(window, keyCallback = new KeyboardHandler(this));
        
        // Sets the mousecallback to my class
        glfwSetCursorPosCallback(window, mousePosCallback = new MousePosInputHandler(this));
        
        glfwSetMouseButtonCallback(window, mouseButtonCallback = new MouseButtonInputHandler(this));
        
        Vector2f location = new Vector2f(0.0f, 0.0f);
        mouseLocation = location;
	}
	
	public void keyChange(int keyCode, int action)
	{		
		
	}
	
	public void MouseButtonChange(int button, int action)
	{		
		if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
		{
			System.out.println("button pressed: " + mouseLocation.toString());
		}
	}
	
	public void MouseLocationChange(Vector2f location)
	{		
		mouseLocation = location;
	}

	public Vector2f getMouseLocation()
	{
		return mouseLocation;
	}
}
