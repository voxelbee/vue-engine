package com.vue.core.input;

import org.lwjgl.glfw.GLFWCursorPosCallback;

import com.vue.core.math.Vector2f;

public class MousePosInputHandler extends GLFWCursorPosCallback
{
	InputHandler inputHandler;

	@Override
	public void invoke(long window, double xpos, double ypos)
	{
		Vector2f location = new Vector2f((float)(xpos), (float)(ypos));
		inputHandler.MouseLocationChange(location);
	}
	
	public MousePosInputHandler(InputHandler input)
	{
		inputHandler = input;
	}
}
