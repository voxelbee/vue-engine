package com.vue.core.input;

import org.lwjgl.glfw.GLFWMouseButtonCallback;

public class MouseButtonInputHandler extends GLFWMouseButtonCallback
{
	InputHandler inputHandler;

	@Override
	public void invoke(long window, int button, int action, int mods)
	{
		inputHandler.MouseButtonChange(button, action);
	}
	
	public MouseButtonInputHandler(InputHandler input)
	{
		inputHandler = input;
	}
}
