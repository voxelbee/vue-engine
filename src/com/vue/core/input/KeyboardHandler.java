package com.vue.core.input;

import org.lwjgl.glfw.GLFWKeyCallback;

public class KeyboardHandler extends GLFWKeyCallback
{
	InputHandler inputHandler;
	
	// The GLFWKeyCallback class is an abstract method that
	// can't be instantiated by itself and must instead be extended
	// 
	@Override
	public void invoke(long window, int key, int scanCode, int action, int mods)
	{
		inputHandler.keyChange(key, action);
	}
	
	public KeyboardHandler(InputHandler input)
	{
		inputHandler = input;
	}
}
