package com.vue.core.math;

public class Vector4f
{
	private float x;
	private float y;
	private float z;
	private float w;
	
	public Vector4f(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	
	public Vector4f()
	{
		// TODO Auto-generated constructor stub
	}

	public float length()
	{
		return (float)Math.sqrt(x * x + y * y + z * z + w * w);
	}
	
	public float dot(Vector4f r)
	{
		return x * r.getX() + y * r.getY() + z * r.getZ() + w * r.getW();
	}
	
	public Vector4f normalize()
	{
		float length = length();
				
		x /= length;
		y /= length;
		z /= length;
		w /= length;
		
		return this;
	}
	
	public void scale(float s)
	{
		x *= s;
		y *= s;
		z *= s;
		w *= s;
	}
	
	public Vector4f rotate()
	{
		return null;
	}
	
	public Vector4f add(Vector4f r)
	{
		return new Vector4f(x + r.getX(), y + r.getY(), z + r.getZ(), w + r.getW());
	}
	
	public Vector4f add(float r)
	{
		return new Vector4f(x + r, y + r, z + r, w + r);
	}
	
	public Vector4f sub(Vector4f r)
	{
		return new Vector4f(x - r.getX(), y - r.getY(), z - r.getZ(), w - r.getW());
	}
	
	public Vector4f sub(float r)
	{
		return new Vector4f(x - r, y - r, z - r, w - r);
	}
	
	public Vector4f mul(Vector4f r)
	{
		return new Vector4f(x * r.getX(), y * r.getY(), z * r.getZ(), w * r.getW());
	}
	
	public Vector4f mul(float r)
	{
		return new Vector4f(x * r, y * r, z * r, w * r);
	}
	
	public Vector4f div(Vector4f r)
	{
		return new Vector4f(x / r.getX(), y / r.getY(), z / r.getZ(), w / r.getW());
	}
	
	public Vector4f div(float r)
	{
		return new Vector4f(x / r, y / r, z / r, w / r);
	}
	
	public void set(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	
	public void set(Vector4f r)
	{
		this.x = r.getX();
		this.y = r.getY();
		this.z = r.getZ();
		this.w = r.getW();
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public float getZ()
	{
		return z;
	}

	public void setZ(float z)
	{
		this.z = z;
	}
	
	public float getW()
	{
		return w;
	}

	public void setW(float w)
	{
		this.w = w;
	}
}
