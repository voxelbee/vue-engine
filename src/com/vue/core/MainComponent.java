package com.vue.core;

import static org.lwjgl.glfw.Callbacks.glfwSetCallback;
import static org.lwjgl.glfw.GLFW.GLFWWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glUseProgram;

import org.lwjgl.Sys;
import org.lwjgl.glfw.GLFWWindowSizeCallback.SAM;

import com.vue.core.Game;
import com.vue.core.Window;
import com.vue.core.input.InputHandler;
import com.vue.core.math.Vector2f;
import com.vue.core.util.RenderUtil;

public class MainComponent
{
	private Window windowManager =  new Window();
	private InputHandler input;
	private boolean isRunning;
	public static int FRAME_CAP = 10000;
	private long variableYieldTime, lastTime;
    private boolean resized;
    private Vector2f screenSize;
        
    private int aspect;
	
	private Game game;
		
	public MainComponent()
	{
		isRunning = false;
	}
	
    private void run()
    {
    	isRunning = true;
    	
    	long frameCounter = 0;
    	long lastTime = 0;
    	long currentTime = 0;
    	long timeDif = 0;
    	
    	while(isRunning)
    	{
    		if(windowManager.isCloseRequested() != GL_FALSE)
    		{
    			stop();
    		}
    		sync(FRAME_CAP);
    		render();
    		game.update();
    		
    		frameCounter++;
    		timeDif = Time.getTime() - lastTime;
    		
    		if(timeDif >= Time.SECOND)
    		{
    			lastTime = Time.getTime();
    			System.out.println(frameCounter);
    			currentTime = 0;
    			frameCounter = 0;
    		}
    		
    		if(resized)
    		{
    			game.screenSizeChange();
    		    glViewport(0, 0, (int)screenSize.getX() / (aspect), (int)screenSize.getY() / (aspect));
    		    resized = false;
    		}
    	}
    	cleanUp();
    }
    
    public void setClose()
    {
    	windowManager.setToClose();
    }
    
    private void sync(int fps)
    {
        if (fps <= 0)
        {
        	return;
        }
          
        long sleepTime = 1000000000 / fps; // nanoseconds to sleep this frame
        // yieldTime + remainder micro & nano seconds if smaller than sleepTime
        long yieldTime = Math.min(sleepTime, variableYieldTime + sleepTime % (1000*1000));
        long overSleep = 0; // time the sync goes over by
          
        try 
        {
            while(true) 
            {
                long t = Time.getTime() - lastTime;
                  
                if(t < sleepTime - yieldTime) 
                {
                    Thread.sleep(1);
                }
                else if (t < sleepTime)
                {
                    // burn the last few CPU cycles to ensure accuracy
                    Thread.yield();
                }
                else
                {
                    overSleep = t - sleepTime;
                    break; // exit while loop
                }
            }
        }
        catch (InterruptedException e) 
        {
            e.printStackTrace();
        }
        finally
        {
            lastTime = Time.getTime() - Math.min(overSleep, sleepTime);
             
            // auto tune the time sync should yield
            if (overSleep > variableYieldTime)
            {
                // increase by 200 microseconds (1/5 a ms)
                variableYieldTime = Math.min(variableYieldTime + 200*1000, sleepTime);
            }
            else if (overSleep < variableYieldTime - 200*1000)
            {
                // decrease by 2 microseconds
                variableYieldTime = Math.max(variableYieldTime - 2*1000, 0);
            }
        }
    }
    
    public void start()
    {  	
    	if(isRunning)
    	{
    		return;
    	}    	
    	//Create the window for the game engine  
    	windowManager.createWindow(false);
    	aspect = windowManager.GetAspect();
    	screenSize = windowManager.getScreenSize();
    	
        glfwSetCallback(windowManager.getWindow(), GLFWWindowSizeCallback(new SAM() 
        {
			@Override
            public void invoke(long window, int width, int height)
			{
                resized = true;
                screenSize.setX(width);
                screenSize.setY(height);
            }
        }));
        
        input = new InputHandler(windowManager.getWindow(), this);
		game = new Game(input, this);
		game.init();
		RenderUtil.init();
		
    	run();
    }
    
    public void stop()
    {
    	if(!isRunning)
    	{
    		return;
    	}
    	
    	isRunning = false;
    }
    
    private void render()
    {  	
    	glfwSwapBuffers(windowManager.getWindow()); // swap the color buffers
    	glfwPollEvents();
    	
    	RenderUtil.clearScreen();
    	game.render();
    }
    
    private void cleanUp()
    {
    	glDisableVertexAttribArray(0);
    	glUseProgram(0);
    	game.destroy();
    	windowManager.destroy();
    }
 
    public static void main(String[] args)
    {
        System.out.println("LWJGL version " + Sys.getVersion());
        
        //Start the engine
        MainComponent engine = new MainComponent();
        engine.start();
    }
    
    public Vector2f getScreenSize()
    {
    	return screenSize;
    }
    
    public long getWindow()
    {
    	return windowManager.getWindow();
    }
}