package com.vue.core;

import com.vue.core.input.InputHandler;
import com.vue.core.math.Vector2f;
import com.vue.core.math.Vector3f;
import com.vue.core.opengl.OpenGLControl;
import com.vue.core.opengl.Quad;
import com.vue.core.opengl.RayController;
import com.vue.core.opengl.Shader;
import com.vue.core.opengl.Vertex;
import com.vue.core.util.CameraUtil;
import com.vue.core.util.VUEUtils;

import demo01.Demo01;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL43.GL_COMPUTE_WORK_GROUP_SIZE;

import java.io.IOException;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;

public class Game
{
	private InputHandler inputHandler;
	private MainComponent main;
	private OpenGLControl openGL = new OpenGLControl();
	private Quad quad;
	private Shader shader;
	private Shader rayShader;
	private RayController rayControl;
	private CameraUtil camera;
	private Vector2f workSize = new Vector2f();
	private int rayProgram;
	private int textureID;
	private Demo01 dem;
	
	private int workGroupSizeX;
	private int workGroupSizeY;
	
	public Game(InputHandler input, MainComponent main)
	{
		this.inputHandler = input;
		this.main = main;
	}
	
	public void input()
	{
		
	}
	
	
	public void update()
	{
		
	}
	
	public void init()
	{		
		openGL.createOpenGL();
		
		shader = new Shader();
		rayShader = new Shader();
		quad = new Quad();
		rayControl = new RayController();
		camera = new CameraUtil();
		//dem = new Demo01();
		
		Vertex[] data = new Vertex[] {new Vertex(new Vector3f(-1f, -1f, 0)),
									  new Vertex(new Vector3f(-1f, 1f, 0)),
									  new Vertex(new Vector3f(1f, -1f, 0)),
									  new Vertex(new Vector3f(1f, 1f, 0)),
									  new Vertex(new Vector3f(-1f, 1f, 0)),
									  new Vertex(new Vector3f(1f, -1f, 0))};
		
		quad.addVertices(data);
		
		shader.addFragmentShader(ResourceLoader.loadShader("Quad.vs"));
		shader.addVertexShader(ResourceLoader.loadShader("Quad.fs"));
		rayProgram = rayShader.addComputeShader(ResourceLoader.loadShader("Rays.glc"));
		rayShader.addUniform("texture0");
		
		shader.addUniform("iResolution");
		shader.addUniform("texture0");
		screenSizeChange();
		
		camera.setLookAt(new Vector3f(3.0f, 2.0f, 7.0f), new Vector3f(0.0f, 0.5f, 0.0f), new Vector3f(0.0f, 1.0f, 0.0f));
	}

	public void render()
	{
		//dem.trace();
		
		rayShader.bindCompute();
		rayControl.setCameraRays(camera, rayShader);
		rayShader.runCompute((int)workSize.getX() / workGroupSizeX, (int)workSize.getY() / workGroupSizeY);
		rayShader.resetCompute();
		
		//glClear(GL_COLOR_BUFFER_BIT);
		shader.bind();
		shader.setUniform2f("iResolution", main.getScreenSize());
		quad.draw();
		shader.resetBind();
	}
	
	public void destroy()
	{
		
	}
	
	public void screenSizeChange()
	{
		textureID = shader.createTexture2D("texture0", (int)main.getScreenSize().getX(), (int)main.getScreenSize().getY());
		rayShader.setTextureID("texture0", textureID);
		shader.setUniformi("texture0", textureID);
		rayShader.setUniformi("texture0", textureID);
		
		IntBuffer workGroupSize = BufferUtils.createIntBuffer(3);
		glGetProgramiv(rayProgram, GL_COMPUTE_WORK_GROUP_SIZE, workGroupSize);
		workGroupSizeX = workGroupSize.get(0);
		workGroupSizeY = workGroupSize.get(1);
		rayControl.initalizeCameraRays(rayShader);
		
		camera.setFrustumPerspective(70.0f, main.getScreenSize().getX() / main.getScreenSize().getY(), 1f, 2f);
		workSize.set(VUEUtils.nextPowerOfTwo((int)main.getScreenSize().getX()), VUEUtils.nextPowerOfTwo((int)main.getScreenSize().getY()));
		
	}
}
