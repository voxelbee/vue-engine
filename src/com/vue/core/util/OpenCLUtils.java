package com.vue.core.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.FloatBuffer;

import org.lwjgl.opencl.CL12;
import org.lwjgl.opencl.CL12GL;

import com.vue.core.opencl.OpenCLControl;

public class OpenCLUtils
{	
	private String loadOpenCLSource(String fileName)
	{
		StringBuilder result = new StringBuilder();
		BufferedReader shaderReader = null;
		
		try
		{			
			shaderReader = new BufferedReader(new FileReader("./Resources/OpenCL/" + fileName));
			String line;
			
			while((line = shaderReader.readLine()) != null)
			{
				result.append(line).append("\n");
			}
			
			shaderReader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
			
		return result.toString();
	}

	public FloatBuffer readData(OpenCLControl openCL, int memoryID, FloatBuffer out) 
	{
		return openCL.readMemoryBankFromDeviceFloat(memoryID, out.capacity(), false);
	}
	
	public void runProgram(OpenCLControl openCL, int program, int readSize)
	{
		openCL.executeProgram(program, readSize);
	}
	
	public int uploadData(OpenCLControl openCL, FloatBuffer buf, int memoryType, int program)
	{
		int memID = openCL.addMemoryBank(buf, memoryType);
		openCL.uploadMemoryBankData(memID, program);		
		return memID;
	}
	
	public int createProgram(OpenCLControl openCL ,String fileName)
	{
		String source = loadOpenCLSource(fileName);
		return openCL.addProgram(source);
	}
	
	public void createOpenCL(OpenCLControl openCL, long glfwWindow)
	{
		openCL.initializeOpenCL(glfwWindow);
	}
	
	public void destroyOpenCL(OpenCLControl openCL)
	{
		openCL.destroyOpenCL();
	}
}
