package com.vue.core.util;

import static org.lwjgl.opengl.GL11.*;

public class RenderUtil
{
	public static void clearScreen()
	{
		glClear(0);
	}
	
	public static void init()
	{
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_2D);
	}
}
