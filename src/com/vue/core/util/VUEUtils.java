package com.vue.core.util;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.nio.DoubleBuffer;
import java.nio.LongBuffer;

import org.lwjgl.BufferUtils;

import com.vue.core.opengl.Vertex;

public class VUEUtils
{
	public static FloatBuffer createFloatBuffer(int size)
	{
		return BufferUtils.createFloatBuffer(size);
	}
	
	public static FloatBuffer createFlipedBuffer(Vertex[] vertices)
	{
		FloatBuffer buffer = createFloatBuffer(vertices.length * Vertex.SIZE);
		
		for(int i = 0; i < vertices.length; i++)
		{
			buffer.put(vertices[i].getPos().getX());
			buffer.put(vertices[i].getPos().getY());
			buffer.put(vertices[i].getPos().getZ());
		}
		
		buffer.flip();
		
		return buffer;
	}
	
	public static FloatBuffer toBuffer(float[] floats)
	{
        FloatBuffer buf = BufferUtils.createFloatBuffer(floats.length).put(floats);
        buf.rewind();
        return buf;
    }
	
	public static DoubleBuffer toBuffer(double[] doubles)
	{
		DoubleBuffer buf = BufferUtils.createDoubleBuffer(doubles.length).put(doubles);
        buf.rewind();
        return buf;
    }
	
	public static LongBuffer toBuffer(long[] longs)
	{
		LongBuffer buf = BufferUtils.createLongBuffer(longs.length).put(longs);
        buf.rewind();
        return buf;
    }
	
	public static IntBuffer toBuffer(int[] ints)
	{
		IntBuffer buf = BufferUtils.createIntBuffer(ints.length).put(ints);
        buf.rewind();
        return buf;
    }
	
	public static ShortBuffer toBuffer(short[] shorts)
	{
		ShortBuffer buf = BufferUtils.createShortBuffer(shorts.length).put(shorts);
        buf.rewind();
        return buf;
    }
	
	public static ByteBuffer toBuffer(byte[] bytes)
	{
		ByteBuffer buf = BufferUtils.createByteBuffer(bytes.length).put(bytes);
        buf.rewind();
        return buf;
    }
	
	public static void printFloatBuffer(FloatBuffer buffer)
	{
        for (int i = 0; i < buffer.capacity(); i++)
        {
            System.out.print(buffer.get(i)+" ");
        }
        System.out.println("");
    }

	public static int nextPowerOfTwo(int x) {
		x--;
		x |= x >> 1; // handle 2 bit numbers
		x |= x >> 2; // handle 4 bit numbers
		x |= x >> 4; // handle 8 bit numbers
		x |= x >> 8; // handle 16 bit numbers
		x |= x >> 16; // handle 32 bit numbers
		x++;
		return x;
	}
}
