package com.vue.core.opencl;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLUtil;
import org.lwjgl.Pointer;
import org.lwjgl.PointerBuffer;
import org.lwjgl.egl.KHRCreateContext;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opencl.CL10.*;
import static org.lwjgl.opengl.GL45.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opencl.CL10GL.*;
import static org.lwjgl.opencl.CLUtil.checkCLError;
import static org.lwjgl.system.MemoryUtil.NULL;
import static org.lwjgl.system.MemoryUtil.memDecodeUTF8;

import org.lwjgl.glfw.*;
import org.lwjgl.opencl.*;
import org.lwjgl.opencl.CLPlatform.Filter;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.system.libffi.Closure;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFWNativeGLX.*;
import static org.lwjgl.glfw.GLFWNativeWGL.*;
import static org.lwjgl.glfw.GLFWNativeX11.*;
import static org.lwjgl.opencl.CLUtil.*;
import static org.lwjgl.opencl.Info.*;
import static org.lwjgl.opencl.KHRGLSharing.*;
import static org.lwjgl.opengl.ARBCLEvent.*;
import static org.lwjgl.opengl.CGL.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.opengl.WGL.*;
import static org.lwjgl.system.MemoryUtil.*;

public class OpenCLControl
{
	// OpenCL variables
	private IntBuffer errcode;
	private PointerBuffer ctxProps;
	private CLPlatform platform;
	private long context;
	private List devices;
	private long queue;
	private List<Long> programs = new ArrayList<Long>();
	private List<Long> memoryBanks = new ArrayList<Long>();
	private List<Long> kernels = new ArrayList<Long>();
	private List<Integer> memoryBanksPerProgram = new ArrayList<Integer>();
	private CLDevice device;
	
	/**
	 * This is the float buffer version of addMemoryBank:
	 * 
	 * Creates a memory bank for the openCL 
	 * context that can be written to by the host
	 * device. This can be accessed by the working 
	 * device. This is a read only memory bank.
	 * 
	 * @param buffer The buffer to add into the memory bank
	 * @param memoryType The type of memory buffer to create. eg. CL_MEM_READ_ONLY
	 * 
	 * @return The ID for the memory bank
	 */
	public int addMemoryBank(FloatBuffer buffer, int memoryType)
	{
		memoryBanks.add(clCreateBuffer(context, memoryType | CL_MEM_COPY_HOST_PTR, buffer, errcode));
		
		try
		{
		    checkCLError(errcode);
		}
		catch(OpenCLException e)
		{
			System.err.println("Error: Failed to create openCL memory bank:");
			e.printStackTrace();
			System.exit(1);
		}
		return memoryBanks.size();
	}
	
	/**
	 * This is the short buffer version of writeToMemoryBank:
	 * 
	 * Writes a buffer to the memory bank for the working 
	 * device use. This buffer has to be the same type 
	 * that the memory bank was initialised and the same size.
	 * Call	clFinish to make sure the upload has finished.
	 * 
	 * @param buffer The buffer data to add into the memory bank
	 * @param memoryBankID The int ID of the memory bank to add the data to
	 */
	public void writeToMemoryBank(FloatBuffer buffer, int memoryBankID)
	{
        clEnqueueWriteBuffer(queue, memoryBanks.get(memoryBankID - 1), 1, 0, buffer, null, null);
	}
	
	/**
	 * This is the short buffer version of addMemoryBank:
	 * 
	 * Creates a memory bank for the openCL 
	 * context that can be written to by the host
	 * device. This can be accessed by the working 
	 * device. This is a read only memory bank.
	 * 
	 * @param buffer The buffer to add into the memory bank
	 * @param memoryType The type of memory buffer to create. eg. CL_MEM_READ_ONLY
	 * 
	 * @return The ID for the memory bank
	 */
	public int addMemoryBank(ShortBuffer buffer, int memoryType)
	{
		memoryBanks.add(clCreateBuffer(context, memoryType | CL_MEM_COPY_HOST_PTR, buffer, errcode));
		
		try
		{
		    checkCLError(errcode);
		}
		catch(OpenCLException e)
		{
			System.err.println("Error: Failed to create openCL memory bank:");
			e.printStackTrace();
			System.exit(1);
		}
		return memoryBanks.size();
	}
	
	/**
	 * This is the short buffer version of writeToMemoryBank:
	 * 
	 * Writes a buffer to the memory bank for the working 
	 * device use. This buffer has to be the same type 
	 * that the memory bank was initialised and the same size.
	 * Call	clFinish to make sure the upload has finished.
	 * 
	 * @param buffer The buffer data to add into the memory bank
	 * @param memoryBankID The int ID of the memory bank to add the data to
	 */
	public void writeToMemoryBank(ShortBuffer buffer, int memoryBankID)
	{
        clEnqueueWriteBuffer(queue, memoryBanks.get(memoryBankID - 1), 1, 0, buffer, null, null);
	}
	
	/**
	 * This is the byte buffer version of addMemoryBank:
	 * 
	 * Creates a memory bank for the openCL 
	 * context that can be written to by the host
	 * device. This can be accessed by the working 
	 * device. This is a read only memory bank.
	 * 
	 * @param buffer The buffer to add into the memory bank
	 * @param memoryType The type of memory buffer to create. eg. CL_MEM_READ_ONLY
	 * 
	 * @return The ID for the memory bank
	 */
	public int addMemoryBank(ByteBuffer buffer, int memoryType)
	{
		memoryBanks.add(clCreateBuffer(context, memoryType | CL_MEM_COPY_HOST_PTR, buffer, errcode));
		
		try
		{
		    checkCLError(errcode);
		}
		catch(OpenCLException e)
		{
			System.err.println("Error: Failed to create openCL memory bank:");
			e.printStackTrace();
			System.exit(1);
		}
		return memoryBanks.size();
	}
	
	/**
	 * This is the byte buffer version of writeToMemoryBank:
	 * 
	 * Writes a buffer to the memory bank for the working 
	 * device use. This buffer has to be the same type 
	 * that the memory bank was initialised and the same size.
	 * Call	clFinish to make sure the upload has finished.
	 * 
	 * @param buffer The buffer data to add into the memory bank
	 * @param memoryBankID The int ID of the memory bank to add the data to
	 */
	public void writeToMemoryBank(ByteBuffer buffer, int memoryBankID)
	{
        clEnqueueWriteBuffer(queue, memoryBanks.get(memoryBankID - 1), 1, 0, buffer, null, null);
	}
	
	/**
	 * This is the double buffer version of addMemoryBank:
	 * 
	 * Creates a memory bank for the openCL 
	 * context that can be written to by the host
	 * device. This can be accessed by the working 
	 * device. This is a read only memory bank.
	 * 
	 * @param buffer The buffer to add into the memory bank
	 * @param memoryType The type of memory buffer to create. eg. CL_MEM_READ_ONLY
	 * 
	 * @return The ID for the memory bank
	 */
	public int addMemoryBank(DoubleBuffer buffer, int memoryType)
	{
		memoryBanks.add(clCreateBuffer(context, memoryType | CL_MEM_COPY_HOST_PTR, buffer, errcode));
		
		try
		{
		    checkCLError(errcode);
		}
		catch(OpenCLException e)
		{
			System.err.println("Error: Failed to create openCL memory bank:");
			e.printStackTrace();
			System.exit(1);
		}
		return memoryBanks.size();
	}
	
	/**
	 * This is the double buffer version of writeToMemoryBank:
	 * 
	 * Writes a buffer to the memory bank for the working 
	 * device use. This buffer has to be the same type 
	 * that the memory bank was initialised and the same size.
	 * Call	clFinish to make sure the upload has finished.
	 * 
	 * @param buffer The buffer data to add into the memory bank
	 * @param memoryBankID The int ID of the memory bank to add the data to
	 */
	public void writeToMemoryBank(DoubleBuffer buffer, int memoryBankID)
	{
        clEnqueueWriteBuffer(queue, memoryBanks.get(memoryBankID - 1), 1, 0, buffer, null, null);
	}
	
	/**
	 * This is the int buffer version of addMemoryBank:
	 * 
	 * Creates a memory bank for the openCL 
	 * context that can be written to by the host
	 * device. This can be accessed by the working 
	 * device. This is a read only memory bank.
	 * 
	 * @param buffer The buffer to add into the memory bank
	 * @param memoryType The type of memory buffer to create. eg. CL_MEM_READ_ONLY
	 * 
	 * @return The ID for the memory bank
	 */
	public int addMemoryBank(IntBuffer buffer, int memoryType)
	{
		memoryBanks.add(clCreateBuffer(context, memoryType | CL_MEM_COPY_HOST_PTR, buffer, errcode));
		
		try
		{
		    checkCLError(errcode);
		}
		catch(OpenCLException e)
		{
			System.err.println("Error: Failed to create openCL memory bank:");
			e.printStackTrace();
			System.exit(1);
		}
		return memoryBanks.size();
	}
	
	/**
	 * This is the int buffer version of writeToMemoryBank:
	 * 
	 * Writes a buffer to the memory bank for the working 
	 * device use. This buffer has to be the same type 
	 * that the memory bank was initialised and the same size.
	 * Call	clFinish to make sure the upload has finished.
	 * 
	 * @param buffer The buffer data to add into the memory bank
	 * @param memoryBankID The int ID of the memory bank to add the data to
	 */
	public void writeToMemoryBank(IntBuffer buffer, int memoryBankID)
	{
        clEnqueueWriteBuffer(queue, memoryBanks.get(memoryBankID - 1), 1, 0, buffer, null, null);
	}
	
	/**
	 * Adds an openCL program from a source string.
	 * It also creates a new kernel for this program.
	 * Your openCL source has to have a method called
	 * "main".
	 * 
	 * @param source The source of the program
	 * @return The program/kernel ID
	 */
	public int addProgram(String source)
	{
		programs.add(clCreateProgramWithSource(context, source, null));
		long program = programs.get(programs.size() - 1);
		
		try
		{
			checkCLError(clBuildProgram(program, device.getPointer(), "-cl-nv-verbose", null, 0L));
		}
		catch(OpenCLException e)
		{
			System.err.println("Error: Failed to create openCL program:");
			e.printStackTrace();
			System.exit(1);
		}
		
		kernels.add(clCreateKernel(program, "main", errcode));
		
		try
		{
			checkCLError(errcode);
		}
		catch(OpenCLException e)
		{
			System.err.println("Error: Failed to create openCL kernel for a program:");
			e.printStackTrace();
			System.exit(1);
		}
		
		memoryBanksPerProgram.add(0);
		
		return programs.size();
	}
	
	/**
	 * Uploads a memory bank to the specified program on the
	 * working device.
	 * 
	 * @param memoryBankID The ID of the memory bank to upload
	 * @param programID The ID for the program to upload the data to
	 * @return The memory bank's index/ID in the specified program.
	 * This is different to the memoryBankID. 
	 */
	public int uploadMemoryBankData(int memoryBankID, int programID)
	{
		int currentPrograms = memoryBanksPerProgram.get(programID - 1);
        clSetKernelArg1p(kernels.get(programID - 1), currentPrograms, memoryBanks.get(memoryBankID - 1));
        memoryBanksPerProgram.set(programID - 1, currentPrograms + 1);
        return currentPrograms;
	}
	
	public int uploadMemoryBankData(long memory, int programID)
	{
		int currentPrograms = memoryBanksPerProgram.get(programID - 1);
        clSetKernelArg1p(kernels.get(programID - 1), currentPrograms, memory);
        memoryBanksPerProgram.set(programID - 1, currentPrograms + 1);
        return currentPrograms;
	}
	
	/**
	 * executes a specified program on the working device
	 * 
	 * @param programID The ID for the program to execute
	 * @param bufferMemorySize The capacity of the memory buffer
	 */
	public void executeProgram(int programID, int bufferMemorySize)
	{
		PointerBuffer kernel1DGlobalWorkSize = BufferUtils.createPointerBuffer(1);
        kernel1DGlobalWorkSize.put(0, bufferMemorySize);
        
        clEnqueueNDRangeKernel(queue, kernels.get(programID - 1), 1, null, kernel1DGlobalWorkSize, null, null, null);
	}
	
	/**
	 * This is the byte buffer version of readMemoryBankFromDevice:
	 * 
	 * Reads a buffer of the working device
	 * 
	 * @param memoryBankID The ID for the program to execute
	 * @param bufferMemorySize The capacity of the memory buffer to read
	 * @param disposeMemory TRUE if you want to get rid of this buffer
	 * on the working device and on the host. After this
	 * the memory bank ID will be old and return a null pointer. 
	 * FALSE to keep the buffer on both devices.
	 */
	public ByteBuffer readMemoryBankFromDeviceByte(int memoryBankID, int bufferMemorySize, boolean disposeMemory)
	{
		ByteBuffer buf = BufferUtils.createByteBuffer(bufferMemorySize);
        clEnqueueReadBuffer(queue, memoryBanks.get(memoryBankID - 1), 1, 0, buf, null, null);
            
        if(disposeMemory)
        {
        	clReleaseMemObject(memoryBanks.get(memoryBankID - 1));
        	memoryBanks.set(memoryBankID - 1, null);
        }
        
        return buf;
	}
	
	/**
	 * This is the short buffer version of readMemoryBankFromDevice:
	 * 
	 * Reads a buffer of the working device
	 * 
	 * @param memoryBankID The ID for the program to execute
	 * @param bufferMemorySize The capacity of the memory buffer to read
	 * @param disposeMemory TRUE if you want to get rid of this buffer
	 * on the working device and on the host. After this
	 * the memory bank ID will be old and return a null pointer. 
	 * FALSE to keep the buffer on both devices.
	 */
	public ShortBuffer readMemoryBankFromDeviceShort(int memoryBankID, int bufferMemorySize, boolean disposeMemory)
	{
		ShortBuffer buf = BufferUtils.createShortBuffer(bufferMemorySize);
        clEnqueueReadBuffer(queue, memoryBanks.get(memoryBankID - 1), 1, 0, buf, null, null);
            
        if(disposeMemory)
        {
        	clReleaseMemObject(memoryBanks.get(memoryBankID - 1));
        	memoryBanks.set(memoryBankID - 1, null);
        }
        
        return buf;
	}
	
	/**
	 * This is the float buffer version of readMemoryBankFromDevice:
	 * 
	 * Reads a buffer of the working device
	 * 
	 * @param memoryBankID The ID for the program to execute
	 * @param bufferMemorySize The capacity of the memory buffer to read
	 * @param disposeMemory TRUE if you want to get rid of this buffer
	 * on the working device and on the host. After this
	 * the memory bank ID will be old and return a null pointer. 
	 * FALSE to keep the buffer on both devices.
	 */
	public FloatBuffer readMemoryBankFromDeviceFloat(int memoryBankID, int bufferMemorySize, boolean disposeMemory)
	{
		FloatBuffer buf = BufferUtils.createFloatBuffer(bufferMemorySize);
        clEnqueueReadBuffer(queue, memoryBanks.get(memoryBankID - 1), 1, 0, buf, null, null);
            
        if(disposeMemory)
        {
        	clReleaseMemObject(memoryBanks.get(memoryBankID - 1));
        	memoryBanks.set(memoryBankID - 1, null);
        }
        
        return buf;
	}
	
	/**
	 * This is the double buffer version of readMemoryBankFromDevice:
	 * 
	 * Reads a buffer of the working device
	 * 
	 * @param memoryBankID The ID for the program to execute
	 * @param bufferMemorySize The capacity of the memory buffer to read
	 * @param disposeMemory TRUE if you want to get rid of this buffer
	 * on the working device and on the host. After this
	 * the memory bank ID will be old and return a null pointer. 
	 * FALSE to keep the buffer on both devices.
	 */
	public DoubleBuffer readMemoryBankFromDeviceDouble(int memoryBankID, int bufferMemorySize, boolean disposeMemory)
	{
		DoubleBuffer buf = BufferUtils.createDoubleBuffer(bufferMemorySize);
        clEnqueueReadBuffer(queue, memoryBanks.get(memoryBankID - 1), 1, 0, buf, null, null);
            
        if(disposeMemory)
        {
        	clReleaseMemObject(memoryBanks.get(memoryBankID - 1));
        	memoryBanks.set(memoryBankID - 1, null);
        }
        
        return buf;
	}
	
	/**
	 * This is the int buffer version of readMemoryBankFromDevice:
	 * 
	 * Reads a buffer of the working device
	 * 
	 * @param memoryBankID The ID for the program to execute
	 * @param bufferMemorySize The capacity of the memory buffer to read
	 * @param disposeMemory TRUE if you want to get rid of this buffer
	 * on the working device and on the host. After this
	 * the memory bank ID will be old and return a null pointer. 
	 * FALSE to keep the buffer on both devices.
	 */
	public IntBuffer readMemoryBankFromDeviceInt(int memoryBankID, int bufferMemorySize, boolean disposeMemory)
	{
		IntBuffer buf = BufferUtils.createIntBuffer(bufferMemorySize);
        clEnqueueReadBuffer(queue, memoryBanks.get(memoryBankID - 1), 1, 0, buf, null, null);
            
        if(disposeMemory)
        {
        	clReleaseMemObject(memoryBanks.get(memoryBankID - 1));
        	memoryBanks.set(memoryBankID - 1, null);
        }
        
        return buf;
	}
	
	/**
	 * Blocks the program until all of the items
	 * in the queue have finished
	 */
	public long createOpenCLTexture(int textureID, int programID)
	{
		long clTexture = clCreateFromGLTexture2D(context, CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0, textureID, errcode);
		checkCLError(errcode);
		glBindTexture(GL_TEXTURE_2D, textureID);
		clFinish(queue);
		return clTexture;
	}
	
	public void setProgramInt(int programID, int value)
	{
		int currentPrograms = memoryBanksPerProgram.get(programID - 1);
		clSetKernelArg1i(kernels.get(programID - 1), currentPrograms, value);
        memoryBanksPerProgram.set(programID - 1, currentPrograms + 1);
	}
	
	public void setProgramPointer(int programID, long clTexture)
	{
		int currentPrograms = memoryBanksPerProgram.get(programID - 1);
		clSetKernelArg1p(kernels.get(programID - 1), currentPrograms, clTexture);
        memoryBanksPerProgram.set(programID - 1, currentPrograms + 1);
	}
	
	public void getGLObjects(long clObjectID)
	{
		int errcode = clEnqueueAcquireGLObjects(queue, clObjectID, null, null);
		checkCLError(errcode);
	}
	
	/**
	 * Blocks the program until all of the items
	 * in the queue have finished
	 */
	public void blockUntilFinished()
	{
        clFinish(queue);
	}
	
	public void createGLCLTexture()
	{
		//clCreateFromGLTexture2D(context, CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, miplevel, texture, errcode_ret)
	}

	public void initializeOpenCL(long glfwWindow)
	{
		System.setProperty("org.lwjgl.opencl.explicitInit","true");
		
		//Initialize OpenCL and create a context and command queue
        CL.create();
        System.out.println("Init: OpenCL created");
        
        platform = CLPlatform.getPlatforms().get(0);
        System.out.println("Init: OpenCL Platform created");
        
		// Find devices with GL sharing support
		Filter<CLDevice> glSharingFilter = new Filter<CLDevice>()
		{
			@Override
			public boolean accept(CLDevice device)
			{
				CLCapabilities caps = device.getCapabilities();
				return caps.cl_khr_gl_sharing || caps.cl_APPLE_gl_sharing;
			}
		};
		
		List<CLDevice> devices = platform.getDevices(CL_DEVICE_TYPE_GPU, glSharingFilter);
		if (devices == null)
		{
			devices = platform.getDevices(CL_DEVICE_TYPE_GPU, glSharingFilter);
			if ( devices == null )
				throw new RuntimeException("No OpenCL devices found with KHR_gl_sharing support.");
		}
		this.device = devices.get(0);
        
        ctxProps = BufferUtils.createPointerBuffer(7);
     	switch (LWJGLUtil.getPlatform())
     	{
     		case WINDOWS:
     			ctxProps
     				.put(CL_GL_CONTEXT_KHR)
     				.put(glfwGetWGLContext(glfwWindow))
     				.put(CL_WGL_HDC_KHR)
     				.put(wglGetCurrentDC());
     			break;
     		case LINUX:
     		    long linuxWindow = GLFWNativeGLX.glfwGetGLXContext(glfwWindow);	
     			ctxProps
     				.put(CL_GL_CONTEXT_KHR)
     				.put(glfwGetGLXContext(linuxWindow))
     				.put(CL_GLX_DISPLAY_KHR)
     				.put(glfwGetX11Display());
     			break;
     		case MACOSX:
     			ctxProps
     				.put(APPLEGLSharing.CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE)
     				.put(CGLGetShareGroup(CGLGetCurrentContext()));
     			break;
     	}
     	ctxProps
     		.put(CL_CONTEXT_PLATFORM)
     		.put(platform)
     		.put(NULL)
     		.flip();
        System.out.println("Init: CTX created");
  
        errcode = BufferUtils.createIntBuffer(1);
        System.out.println("Init: ERRCODE created");
        
        devices = platform.getDevices(CL_DEVICE_TYPE_GPU);
        //long context = clCreateContext(platform, devices, null, null, null);
        context = clCreateContext(ctxProps, device.getPointer(), CREATE_CONTEXT_CALLBACK, NULL, errcode);
        System.out.println("Init: OpenCl context created");
   
        checkCLError(errcode);
        queue = clCreateCommandQueue(context, device.getPointer(), CL_QUEUE_PROFILING_ENABLE, errcode);
        System.out.println("Init: OpenCL Command queue created");
	}


	/**
	 * Cleans up openCL and frees up the memory on the 
	 * host and working device.
	 */
	public void destroyOpenCL()
	{				
		// Release the kernels on the openCL device
        for(int i = 0; i < programs.size(); i++)
        {
    		clReleaseKernel(kernels.get(i));
        }
        kernels.clear();
		
		// Release the programs on the openCl device
        for(int i = 0; i < programs.size(); i++)
        {
            clReleaseProgram(programs.get(i));
        }
        programs.clear();
        
        // Release the memory banks on the openCL device
        for(int i = 0; i < memoryBanks.size(); i++)
        {
            clReleaseMemObject(memoryBanks.get(i));
        }
        memoryBanks.clear();  
        
        clReleaseCommandQueue(queue);
        clReleaseContext(context);
		CL.destroy();
	}
	
	private static final CLCreateContextCallback CREATE_CONTEXT_CALLBACK = new CLCreateContextCallback()
	{
		  @Override
		  public void invoke(long errinfo, long private_info, long cb, long user_data) 
		  {
		   System.err.println("Error: Couldn't find OpenCL source. Please update your graphics drivers");
		   System.err.println("\tMoreInfo: " + memDecodeUTF8(errinfo));
		  }
	};
}
