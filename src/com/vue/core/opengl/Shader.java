package com.vue.core.opengl;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.GL_RGBA32F;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.opengl.GL42.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT;
import static org.lwjgl.opengl.GL42.glBindImageTexture;
import static org.lwjgl.opengl.GL42.glMemoryBarrier;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.GL_READ_WRITE;
import static org.lwjgl.opengl.GL15.GL_WRITE_ONLY;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL43.*;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;

import com.vue.core.math.Vector2f;
import com.vue.core.math.Vector3f;

public class Shader
{
	private int program;
	private Map<String, Integer> textures;
	private Map<String, Integer> uniforms;
	
	public Shader()
	{
		program = glCreateProgram();
		uniforms = new HashMap<String, Integer>();
		textures = new HashMap<String, Integer>();
		
		if(program == 0)
		{
			System.err.println("Error: Shader creation failed Could not find valid memory loaction in constructor");
			System.exit(1);
		}
	}
	
	public void bind()
	{
		glUseProgram(program);
		glBindTexture(GL_TEXTURE_2D, textures.get("texture0"));
	}
	
	public void bindCompute()
	{
		glUseProgram(program);
		glBindImageTexture(0, textures.get("texture0"), 0, false, 0, GL_WRITE_ONLY, GL_RGBA32F);
	}
	
	public void resetCompute()
	{
		glBindImageTexture(0, 0, 0, false, 0, GL_READ_WRITE, GL_RGBA32F);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
		glUseProgram(0);
	}
	

	public void resetBind()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
		glUseProgram(0);		
	}
	
	public void runCompute(int numberOfGroupsX, int numberOfGroupsY)
	{
		glDispatchCompute(numberOfGroupsX, numberOfGroupsY, 1);
	}
	
	public void addUniform(String uniform)
	{
		int uniformLocation = glGetUniformLocation(program, uniform);
		
		if(uniformLocation == 0xFFFFFFF)
		{
			System.err.println("Error: Could not find uniform: " + uniform);
			new Exception().printStackTrace();
			System.exit(1);
		}
		
		uniforms.put(uniform, uniformLocation);
	}
	
	public void addVertexShader(String text)
	{
		createShaderProgramme(text, GL_VERTEX_SHADER);
	}
	
	public void addGeometryShader(String text)
	{
		createShaderProgramme(text, GL_GEOMETRY_SHADER);
	}
	
	public void addFragmentShader(String text)
	{
		createShaderProgramme(text, GL_FRAGMENT_SHADER);
	}
	
	public int addComputeShader(String text)
	{
		return createShaderProgramme(text, GL_COMPUTE_SHADER);
	}
	
	int CreateShader(String shaderString, int shadertype)
	{
	    int shader = glCreateShader(shadertype);
	    glShaderSource(shader, shaderString);
	    glCompileShader(shader);
	    int status = glGetShaderi(shader, GL_COMPILE_STATUS);
	    if (status == GL_FALSE)
	    {
	        
	        String error = glGetShaderInfoLog(shader);
	        
	        String ShaderTypeString = null;
	        switch(shadertype)
	        {
	        case GL_VERTEX_SHADER: ShaderTypeString = "vertex"; break;
	        case GL_GEOMETRY_SHADER: ShaderTypeString = "geometry"; break;
	        case GL_FRAGMENT_SHADER: ShaderTypeString = "fragment"; break;
	        case GL_COMPUTE_SHADER: ShaderTypeString = "compute"; break;
	        }
	        
	        System.err.println("Error: Compile failure in " + ShaderTypeString + " shader: \n" + error +"\n");
	    }
	    return shader;
	}

	public int createShaderProgramme(String text, int shader)
	{
		int shaderid = CreateShader(text, shader);
	    return createShaderProgramme(shaderid);
	}

	public int createShaderProgramme(int shader)
	{	    
		glAttachShader(program, shader);
	    glLinkProgram(program);
	    
	    int status = glGetShaderi(program, GL_LINK_STATUS);
	    
	    if (status == GL_FALSE)
	    {
	        String error=glGetProgramInfoLog(program);
	        System.err.println("Error: Linker failure: " + error +"\n");
	    }
	    //glDetachShader(program, shader);
	    return program;
	}
	
	public void setUniformi(String uniformName, int value)
	{
		glUniform1i(uniforms.get(uniformName), value);
	}
	
	public void setUniformf(String uniformName, float value)
	{
		glUniform1f(uniforms.get(uniformName), value);
	}
	
	public void setUniform3f(String uniformName, Vector3f value)
	{
		glUniform3f(uniforms.get(uniformName), value.getX(), value.getY(), value.getZ());
	}
	
	public void setUniform2f(String uniformName, Vector2f value)
	{
		glUniform2f(uniforms.get(uniformName), value.getX(), value.getY());
	}
	
	public void createTexture3f(String textureName, int textureSizeX, int textureSizeY, int textureSizeZ, ByteBuffer data)
	{
		int texId = glGenTextures();
		textures.put(textureName, texId);
		//glActiveTexture(GL_TEXTURE0);
		 
		// All RGB bytes are aligned to each other and each component is 1 byte
		//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		
		glBindTexture(GL_TEXTURE_3D, texId);
		
		ByteBuffer image = BufferUtils.createByteBuffer((textureSizeY * textureSizeX * textureSizeZ) * 4);
		for(int i = 0; i < textureSizeX; i++)
		{
			for(int j = 0; j < textureSizeY; j++)
			{
				for(int k = 0; k < textureSizeZ; k++)
				{
					image.put((byte) (Math.random()*255));
					image.put((byte) (Math.random()*255));
					image.put((byte) (Math.random()*255));
					image.put((byte) (255));
				}
			}
		}
		image.flip();
		
		glCompressedTexImage3D(GL_TEXTURE_3D, 0, GL_COMPRESSED_RGBA, textureSizeX, textureSizeY, textureSizeZ, 0, image);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public int createTexture2D(String textureName, int textureSizeX, int textureSizeY)
	{
		int texId = glGenTextures();
		textures.put(textureName, texId);
		
		glBindTexture(GL_TEXTURE_2D, texId);
		
		ByteBuffer black = null;
		
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, textureSizeX, textureSizeY, 0, GL_RGBA, GL_UNSIGNED_BYTE, black);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		return texId;
	}
	
	public int getTextureID()
	{
		return textures.get("texture0");
	}
	
	public void setTextureID(String name, int value)
	{
		textures.put(name, value);
	}
}
