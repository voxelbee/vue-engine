package com.vue.core.opengl;

import org.lwjgl.opengl.GL;

public class OpenGLControl
{	
	public void createOpenGL()
	{
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the ContextCapabilities instance and makes the OpenGL
        // bindings available for use.
    	GL.createCapabilities();
	}
}
