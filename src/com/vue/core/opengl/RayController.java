package com.vue.core.opengl;

import com.vue.core.math.Vector3f;
import com.vue.core.util.CameraUtil;

public class RayController
{
	
	private Vector3f temp = new Vector3f();
	private final Vector3f eyeRay = new Vector3f();
	
	public void initalizeCameraRays(Shader rayShader)
	{
		rayShader.addUniform("eye");
		rayShader.addUniform("ray00");
		rayShader.addUniform("ray10");
		rayShader.addUniform("ray01");
		rayShader.addUniform("ray11");
	}
	
	public void setCameraRays(CameraUtil camera, Shader rayShader)
	{
		temp.set(camera.getPosition().getX(), camera.getPosition().getY(), camera.getPosition().getZ());
		rayShader.setUniform3f("eye", temp);
		
		camera.getEyeRay(-1, -1, eyeRay);
		temp.set(eyeRay.getX(), eyeRay.getY(), eyeRay.getZ());
		rayShader.setUniform3f("ray00", temp);
		
		camera.getEyeRay(-1, 1, eyeRay);
		temp.set(eyeRay.getX(), eyeRay.getY(), eyeRay.getZ());
		rayShader.setUniform3f("ray01", temp);
		
		camera.getEyeRay(1, -1, eyeRay);
		temp.set(eyeRay.getX(), eyeRay.getY(), eyeRay.getZ());
		rayShader.setUniform3f("ray10", temp);
		
		camera.getEyeRay(1, 1, eyeRay);
		temp.set(eyeRay.getX(), eyeRay.getY(), eyeRay.getZ());
		rayShader.setUniform3f("ray11", temp);
	}
}
