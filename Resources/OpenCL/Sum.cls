kernel void main(const int width, const int height, __write_only image2d_t output) 
{
	unsigned int ix = get_global_id(0);
	unsigned int iy = get_global_id(1);
	//uint c = colorMap[colorIndex];
    write_imageui(output, (int2)(ix, iy), (uint4)(ix, ix + iy, iy, iy + ix));
}