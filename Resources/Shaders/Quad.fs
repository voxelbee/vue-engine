#version 410 core

/* This comes interpolated from the vertex shader */
in vec2 texcoord;
in vec2 gl_FragCoord;

/* The fragment color */
out vec4 color;

/* The texture we are going to sample */
uniform sampler2D texture0;
uniform vec2 iResolution;

void main(void)
{
  /* Well, simply sample the texture */
  //color = texture(texture0, texcoord);
  color = vec4(1,1, 1, 0);
}